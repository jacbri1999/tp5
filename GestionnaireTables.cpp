/********************************************
* Titre: Travail pratique #5 - gestionnaireTables.cpp
* Date: 21 mars 2019
* Auteur: Moussa Traor� & Ryan Hardie & Wassim Khene
*******************************************/


#include "GestionnaireTables.h"
#include "LectureFichierEnSections.h"

GestionnaireTables::~GestionnaireTables() {
	auto end = conteneur_.end();
	auto it = conteneur_.begin();
	for (it; it != end; it++)
		delete *it;
	conteneur_.clear();
}



Table * GestionnaireTables::getTable(int id) const //PAS SUR
{
	auto end = conteneur_.end();
	auto it = conteneur_.begin();
	for (it; it != end; it++)
		if ((*it)->getId() == id)
			return *it;
	return nullptr;
}

Table * GestionnaireTables::getMeilleureTable(int tailleGroupe) const
{
	Table* meilleureTable = nullptr;
	auto end = conteneur_.end();
	auto it = conteneur_.begin();
	for (it; it != end; it++){
		if (!((*it)->estOccupee()) && (*it)->getId() != ID_TABLE_LIVRAISON) {
			int placesACetteTable = (*it)->getNbPlaces();
			if (placesACetteTable >= tailleGroupe && (!meilleureTable || placesACetteTable < meilleureTable->getNbPlaces()))
				meilleureTable = *it;
		}
	}
	return meilleureTable;
}

void GestionnaireTables::lireTables(const string& nomFichier)
{
	LectureFichierEnSections fichier{ nomFichier };
	fichier.allerASection("-TABLES");
	while (!fichier.estFinSection()) {
		int id, nbPlaces;
		fichier >> id >> nbPlaces;
		ajouter(new Table(id, nbPlaces));
	}
}

GestionnaireTables & GestionnaireTables::operator+=(Table * table)
{
	ajouter(table);
	return *this;
}

void GestionnaireTables::afficherTables(ostream & os) const
{
	auto end = conteneur_.end();
	auto it = conteneur_.begin();
	for (it; it != end; it++)
		os << **it;
}



