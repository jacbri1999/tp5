/********************************************
* Titre: Travail pratique #5 - Foncteur.h
* Date: 21 mars 2019
* Auteur: Moussa Traor� & Ryan Hardie & Wassim Khene
*******************************************/

#pragma once

#include <algorithm>
#include "Plat.h"

using namespace std;


class FoncteurPlatMoinsCher
{ // TODO
public:
	FoncteurPlatMoinsCher() {};
	bool operator()(const pair<string, Plat*>& paire1, const pair<string, Plat*>& paire2) {
		return (*(paire1.second) < *(paire2.second));
	}
};

class FoncteurIntervalle
{
   // TODO
public:
	FoncteurIntervalle(double borneInf, double borneSup) : borneInf_(borneInf), borneSup_(borneSup) {};
	bool operator()(const pair<string, Plat*>& paire1) {
		return (paire1.second->getPrix() > borneInf_ && paire1.second->getPrix() < borneSup_); // peut etre plus petit egal
	}
private:
	double borneInf_;
	double borneSup_;
};

