/********************************************
* Titre: Travail pratique #5 - gestionnairePlats.cpp
* Date: 21 mars 2019
* Auteur: Moussa Traor� & Ryan Hardie & Wassim Khene
*******************************************/

#include "GestionnairePlats.h"
#include "LectureFichierEnSections.h"
#include "PlatBio.h"
#include "PlatVege.h"
#include "PlatBioVege.h"



void GestionnairePlats::lirePlats(const string& nomFichier, TypeMenu type)
{
	LectureFichierEnSections fichier{ nomFichier };
	fichier.allerASection(entetesDesTypesDeMenu[static_cast<int>(type)]);
	while (!fichier.estFinSection())
		ajouter(lirePlatDe(fichier));
}

pair<string, Plat*> GestionnairePlats::lirePlatDe(LectureFichierEnSections& fichier)
{
	auto lectureLigne = fichier.lecteurDeLigne();
	Plat* plat;
	string nom, typeStr;
	TypePlat type;
	double prix, coutDeRevient;
	lectureLigne >> nom >> typeStr >> prix >> coutDeRevient;
	type = TypePlat(stoi(typeStr));
	double ecotaxe, vitamines, proteines, mineraux;
	switch (type) {
	case TypePlat::Bio:
		lectureLigne >> ecotaxe;
		plat = new PlatBio{ nom, prix, coutDeRevient, ecotaxe };
		break;
	case TypePlat::BioVege:
		lectureLigne >> ecotaxe >> vitamines >> proteines >> mineraux;
		plat = new PlatBioVege(nom, prix, coutDeRevient, ecotaxe, vitamines, proteines, mineraux);
		break;
	case TypePlat::Vege:
		lectureLigne >> vitamines >> proteines >> mineraux;
		plat = new PlatVege(nom, prix, coutDeRevient, vitamines, proteines, mineraux);
		break;
	default:
		plat = new Plat{ nom, prix, coutDeRevient };
	}
	return pair<string, Plat*>(plat->getNom(), plat);
}

// La methode afficherPlats prend en parametre un ostream et a pour but d'afficher les informations importantes 
// des plats. On parcours tous les elements du conteneur et on appelle afficherPlat pour chaque pointeur de plat, 
// soit le deuxieme element de la pair. 
void GestionnairePlats::afficherPlats(ostream & os)
{
	map<string, Plat*>::iterator end = conteneur_.end();
	map<string, Plat*>::iterator it = conteneur_.begin();
	for (it; it != end; it++)
		it->second->afficherPlat(os);
}


// Constructeur par parametre de GestionnairePlats qui prend en parametre un nom de ficheir
// ainsi qu'un type de menu. 
GestionnairePlats::GestionnairePlats(const string & nomFichier, TypeMenu type) : type_(type)
{
	lirePlats(nomFichier, type); // Ajoute des plats afin de constuire le Gestionanire de Plats
}

// Constructeur par copie qui prend en parametre un GestionnairePLats. On parcours tous les elements
// dans le conteneur et on l'ajoute dans le gestionnaire a l'aide de la methode ajouter(paire).
GestionnairePlats::GestionnairePlats(GestionnairePlats * gestionnaire)
{
	type_ = gestionnaire->getType();
	map<string, Plat*>::iterator end = (gestionnaire->getConteneur()).end(); // Retourne un pointeur qui pointe apres le dernier element
	map<string, Plat*>::iterator it = (gestionnaire->getConteneur()).begin(); // Retourne un pointeur qui pointe vers le premier element
	for (it; it != end; it++) {
		pair<string, Plat*> paire = { it->first, allouerPlat(it->second) };
		ajouter(paire);
	}
}

// Desctructeur de GestionnairePlats. On parcours tous les elements du conteneur et on delete le pointeur
// vers le plat, soit le deuxieme element de la pair.
GestionnairePlats::~GestionnairePlats()
{
	map<string, Plat*>::iterator end = conteneur_.end();
	map<string, Plat*>::iterator it = conteneur_.begin();
	for (it; it != end; it++) 
		delete (it->second); // On delete le deuxieme element de la pair (Plats*)
	conteneur_.clear();
}

// La methode getType prend rien en parametre et retourne le type de Menu
TypeMenu GestionnairePlats::getType() const
{
	return type_;
}

// La methode allouerPlat prend en parametre un pointeur vers un plat et copie ce pointeur
// en construisant un nouveu pointeur avec le plat passer en parametre.
Plat * GestionnairePlats::allouerPlat(Plat* plat)
{
	return (new Plat(*plat));
	
}

// La methode trouverPlatMoinsCher prend rien en parametre et retourne un pointeur de Plat. L'algorithme de la STL
// min_element parcours le conteneur au complet en comparant chaque pair ensemble. La methode de comparaison
// est determiner par FoncteurPlatMoinsCher qui a pour but de trouver le plats le moin chere.
Plat * GestionnairePlats::trouverPlatMoinsCher() const
{
	auto it = min_element(conteneur_.begin(), conteneur_.end(), FoncteurPlatMoinsCher());
	return it->second; // Retourne le deuxieme element de la pair, soit un pointeur vers un plat.
}

// La methode trouverPlatPlusCher prend rien en parametre et retourne un pointeur de Plat. L'algorithme de la STl
// max_element parcours le conteneur au complet en comparant chaque pair ensemble. La methode de comapraison est
// determiner par la fonction lambda comparaisonPlat. Cette fonction lambda prend en parametre deux paire et 
// retourne vrai si la paire1 est moin chere que la paire2. 
Plat * GestionnairePlats::trouverPlatPlusCher() const
{
	auto comparaisonPlat = [](const pair<string, Plat*>& paire1, const pair<string, Plat*>& paire2) {return(*(paire1.second) < *(paire2.second)); };
	auto it = max_element(conteneur_.begin(), conteneur_.end(), comparaisonPlat);
	return it->second; // Retourne le deuxieme element de la pair, soit un pointeur vers un plat.
}

// La methode trouverPlat prend en parametre un nom et retourne un pointeur vers un plat. L'algorithme de la STL
// find() prend en parametre un nom et retourne la pair donc le nom est pareille a celui passer en parametre.
Plat * GestionnairePlats::trouverPlat(const string & nom) const
{
	 return (conteneur_.find(nom))->second;
	
}

// La methode getPlatsEntre prend en parametre une borne inferieur ainsi qu'une borne superieur et retourne
// un vecteur contenant des pairs dont les prix sont compris entre la borne inferieur et la borne superieur.
// Le foncteur FoncteurIntervalle determine les plats donc le prix est correct et le copy_if ajoute les pairs 
// si la comparaison est bonne.
vector<pair<string, Plat*>> GestionnairePlats::getPlatsEntre(double borneInf, double borneSup)
{
	vector<pair<string, Plat*>> platsBonPrix;
	FoncteurIntervalle comparaison = FoncteurIntervalle(borneInf, borneSup);
	copy_if(conteneur_.begin(), conteneur_.end(), back_inserter(platsBonPrix), comparaison);
	return platsBonPrix;
}
